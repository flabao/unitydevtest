﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchRay : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    RaycastHit hit;
    Ray ray;

    void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log(" you clicked on " + hit.collider.gameObject.name);
            GameObject touchedObj = hit.collider.gameObject;

            if (hit.collider.gameObject.name == "btnX")
            {
                print(touchedObj);
            }
        }
    }
}
