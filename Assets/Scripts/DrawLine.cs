﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour {

    private LineRenderer lineRenderer;
    private float counter;
    private float dist;

    public Transform origin;
    public Transform destination;

    public float lineDrawSpeed = 6f;

    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        /*
        print("got the awake part");
        lineRenderer.SetPosition(0, origin.position);
        lineRenderer.SetWidth(0.03f, 0.03f);

        dist = Vector3.Distance(origin.position, destination.position);
        */
    }

	// Use this for initialization
	void Start () {

        if (origin == null || destination == null)
            throw new System.Exception("You must assign 2 objects");
        
	}
	
	// Update is called once per frame
	void Update () {
        /*
		if(counter<dist)
        {
            print("got past conditional");
            counter += 0.1f / lineDrawSpeed;
            float x = Mathf.Lerp(0, dist, counter);
            Vector3 pointA = origin.position;
            Vector3 pointB = destination.position;

            //Get the unit vector in the desired direction; multiply the desired length and add the starting point
            Vector3 pointAlongLine = x * Vector3.Normalize(pointB - pointA) + pointA;

            lineRenderer.SetPosition(0, pointA);
            //lineRenderer.SetPosition(1, pointAlongLine);
        }*/

        Vector3 newOriginPos = transform.position + origin.position;
        Vector3 newDestPos = transform.position + destination.position;

        lineRenderer.SetPosition(0, newOriginPos);
        lineRenderer.SetPosition(1, newDestPos);
    }
}
