﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour {

    public Transform TextDescription;

    string btnName;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.touchCount>0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit hit;
            if(Physics.Raycast(ray,out hit))
            {
                btnName = hit.transform.name;

                switch (btnName)
                {
                    case "btnCross":
                        TextDescription.GetComponent<Text>().text = "The X button on Playstation\n is usually used\n for confirmation in menus\n and jumping in games";
                        break;
                    case "btnSquare":
                        TextDescription.GetComponent<Text>().text = "The square button usually\n assigned as light attack\n or sub menu button";
                        break;
                    case "btnTriangle":
                        TextDescription.GetComponent<Text>().text = "The triangle button is\n the most fluid, frequently used\n for accessing alternative views\n in menus or rarely used gameplay moves";
                        break;
                    case "btnCircle":
                        TextDescription.GetComponent<Text>().text = "The circle button is\n frequently used to go back";
                        break;
                    case "btnXBOX":
                        TextDescription.GetComponent<Text>().text = "Press the XBOX button if\n you want to go to the home menu\n or access quick items";
                        break;
                    case "btnX":
                        TextDescription.GetComponent<Text>().text = "The X button on XBOX\n is usually used as a light attack button\n or mode changer button";
                        break;
                    case "btnY":
                        TextDescription.GetComponent<Text>().text = "The Y button is the triangle\n button of the XBOX";
                        break;
                    case "btnA":
                        TextDescription.GetComponent<Text>().text = "The A button is the confirmation button,\n unlike Nintendo it is on \nthe bottom of the diamond shape";
                        break;
                    case "btnB":
                        TextDescription.GetComponent<Text>().text = "The B button is the back button";
                        break;
                    default:

                        break;
                }
            }
        }
	}
}
